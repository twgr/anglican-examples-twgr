;; gorilla-repl.fileformat = 1

;; **
;;; # Bayes Nets
;;; 
;; **

;; @@
(ns bayes-net-workshop
  (:require [gorilla-plot.core :as plot])
  (:use clojure.repl
        [anglican core runtime emit [state :only [get-predicts]]] 
        [anglib crp]
        [clojure.string :only (join split blank?)]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Bayes nets are easy to express in Anglican.    Take, for example, a pedagogical Bayes net
;;; 
;;; <img src="http://www.robots.ox.ac.uk/~fwood/anglican/examples/bayes_net/bayes_net.png" alt="Bayes net graphical model" style="width: 300px;" />.  
;;; 
;;; It's encoding as a program follows.
;;; 
;;; Note that like in the  immediately following code we use  ```dirac``` to inject observations however this is inefficient because of the general purpose algorithms used; most effectively become rejection samplers if the Bayes net is coded this way.  Fine for small Bayes nets like this one, but disastrous in larger nets.
;;; 
;;; Far preferable practice follows.
;; **

;; @@
(defdist dirac
  "Dirac distribution"
  [x] []
      (sample [this] x)
      (observe [this value] (if (= x value) 0.0 (- (/ 1.0 0.0)))))

(with-primitive-procedures [dirac] (defquery bayes-net-natural [] 
  (let [is-cloudy (sample (flip 0.5))
        
        is-raining (cond (= is-cloudy true ) 
                           (sample (flip 0.8))
                         (= is-cloudy false) 
                           (sample (flip 0.2)))
        
        sprinkler  (cond (= is-cloudy true ) 
                           (sample (flip 0.1))
                         (= is-cloudy false) 
                           (sample (flip 0.5)))
        
        wet-grass  (cond (and (= sprinkler true) 
                              (= is-raining true))  								  				    (sample (flip 0.99))
                         (and (= sprinkler false) 
                             (= is-raining false))  								  				    (sample (flip 0.0))
                         (or  (= sprinkler true) 
                              (= is-raining true))  								  					(sample (flip 0.9)))]
    
    	   (observe (dirac sprinkler) true)
           (observe (dirac wet-grass) true)
           

           (predict :s (hash-map :is-cloudy  is-cloudy 
                                 :is-raining is-raining 
                                 :sprinkler  sprinkler)))))  
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;bayes-net/bayes-net-natural</span>","value":"#'bayes-net/bayes-net-natural"}
;; <=

;; **
;;; Let's run the query and see what the probability of it raining is given that the sprinkler is on and the grass is wet anyway.
;; **

;; @@
(->> (doquery :pimh bayes-net-natural nil :number-of-particles 100)
     (map get-predicts)
     (map :s)
     (map :is-raining)
     (map #(if % 1 0))
     (take 10000)
     (#(plot/histogram % :normalize :probability))
     )
     
;; @@
;; =>
;;; {"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"337e7657-0e86-49ad-ae56-e7a3e65f96a8","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"337e7657-0e86-49ad-ae56-e7a3e65f96a8","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"337e7657-0e86-49ad-ae56-e7a3e65f96a8"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"337e7657-0e86-49ad-ae56-e7a3e65f96a8","values":[{"x":0.0,"y":0},{"x":0.06666666666666668,"y":0.6824},{"x":0.13333333333333336,"y":0.0},{"x":0.20000000000000004,"y":0.0},{"x":0.2666666666666667,"y":0.0},{"x":0.33333333333333337,"y":0.0},{"x":0.4,"y":0.0},{"x":0.4666666666666667,"y":0.0},{"x":0.5333333333333333,"y":0.0},{"x":0.6,"y":0.0},{"x":0.6666666666666666,"y":0.0},{"x":0.7333333333333333,"y":0.0},{"x":0.7999999999999999,"y":0.0},{"x":0.8666666666666666,"y":0.0},{"x":0.9333333333333332,"y":0.0},{"x":0.9999999999999999,"y":0.0},{"x":1.0666666666666667,"y":0.3176},{"x":1.1333333333333333,"y":0}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"337e7657-0e86-49ad-ae56-e7a3e65f96a8\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"337e7657-0e86-49ad-ae56-e7a3e65f96a8\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"337e7657-0e86-49ad-ae56-e7a3e65f96a8\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"337e7657-0e86-49ad-ae56-e7a3e65f96a8\", :values ({:x 0.0, :y 0} {:x 0.06666666666666668, :y 0.6824} {:x 0.13333333333333336, :y 0.0} {:x 0.20000000000000004, :y 0.0} {:x 0.2666666666666667, :y 0.0} {:x 0.33333333333333337, :y 0.0} {:x 0.4, :y 0.0} {:x 0.4666666666666667, :y 0.0} {:x 0.5333333333333333, :y 0.0} {:x 0.6, :y 0.0} {:x 0.6666666666666666, :y 0.0} {:x 0.7333333333333333, :y 0.0} {:x 0.7999999999999999, :y 0.0} {:x 0.8666666666666666, :y 0.0} {:x 0.9333333333333332, :y 0.0} {:x 0.9999999999999999, :y 0.0} {:x 1.0666666666666667, :y 0.3176} {:x 1.1333333333333333, :y 0})}], :width 400, :height 247.2188, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}
;; <=

;; **
;;; It looks like the probability of it raining is around 30%.
;;; 
;;; A method of coding this example that will result in higher inference performance looks like the following where we have moved observes as high up in the program as they can be moved.  This is a manual program transformation that will make its way into the compiler at some point.
;; **

;; @@
(defquery bayes-net-alt [] 
  (let [is-cloudy (sample (flip 0.5))
        
        is-raining (cond (= is-cloudy true ) 
                           (sample (flip 0.8))
                         (= is-cloudy false) 
                           (sample (flip 0.2)))
        sprinkler-dist (cond (= is-cloudy true) 
                            	(flip 0.1)
                         	(= is-cloudy false) 
                            	(flip 0.5))
        
        sprinkler true
        _ (observe sprinkler-dist sprinkler)
        
        wet-grass-dist (cond (and (= sprinkler true) 
                             (= is-raining true))  								  				       (flip 0.99)
                        (and (= sprinkler false) 
                             (= is-raining false))  								  				   (flip 0.0)
                        (or  (= sprinkler true) 
                             (= is-raining true))  								  					   (flip 0.9))
        
        wet-grass true
        _ (observe wet-grass-dist wet-grass)]
    		(def x 6)
    
           (predict :s (hash-map :is-cloudy  is-cloudy 
                                 :is-raining is-raining 
                                 :sprinkler  sprinkler) )))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;bayes-net/bayes-net-alt</span>","value":"#'bayes-net/bayes-net-alt"}
;; <=

;; @@
(->> (doquery :pimh bayes-net-alt nil :number-of-particles 10)
     (map get-predicts)
     (map :s)
     (map :is-raining)
     (map #(if % 1 0))
     (take 2000)
     (#(plot/histogram % :normalize :probability))
     )
;; @@
;; =>
;;; {"type":"vega","content":{"axes":[{"scale":"x","type":"x"},{"scale":"y","type":"y"}],"scales":[{"name":"x","type":"linear","range":"width","zero":false,"domain":{"data":"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2","field":"data.x"}},{"name":"y","type":"linear","range":"height","nice":true,"zero":false,"domain":{"data":"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2","field":"data.y"}}],"marks":[{"type":"line","from":{"data":"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2"},"properties":{"enter":{"x":{"scale":"x","field":"data.x"},"y":{"scale":"y","field":"data.y"},"interpolate":{"value":"step-before"},"fill":{"value":"steelblue"},"fillOpacity":{"value":0.4},"stroke":{"value":"steelblue"},"strokeWidth":{"value":2},"strokeOpacity":{"value":1}}}}],"data":[{"name":"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2","values":[{"x":0.0,"y":0},{"x":0.08333333333333336,"y":0.6765},{"x":0.1666666666666667,"y":0.0},{"x":0.25000000000000006,"y":0.0},{"x":0.3333333333333334,"y":0.0},{"x":0.4166666666666668,"y":0.0},{"x":0.5000000000000001,"y":0.0},{"x":0.5833333333333335,"y":0.0},{"x":0.6666666666666669,"y":0.0},{"x":0.7500000000000002,"y":0.0},{"x":0.8333333333333336,"y":0.0},{"x":0.916666666666667,"y":0.0},{"x":1.0000000000000002,"y":0.3235},{"x":1.0833333333333335,"y":0}]}],"width":400,"height":247.2187957763672,"padding":{"bottom":20,"top":10,"right":10,"left":50}},"value":"#gorilla_repl.vega.VegaView{:content {:axes [{:scale \"x\", :type \"x\"} {:scale \"y\", :type \"y\"}], :scales [{:name \"x\", :type \"linear\", :range \"width\", :zero false, :domain {:data \"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2\", :field \"data.x\"}} {:name \"y\", :type \"linear\", :range \"height\", :nice true, :zero false, :domain {:data \"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2\", :field \"data.y\"}}], :marks [{:type \"line\", :from {:data \"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2\"}, :properties {:enter {:x {:scale \"x\", :field \"data.x\"}, :y {:scale \"y\", :field \"data.y\"}, :interpolate {:value \"step-before\"}, :fill {:value \"steelblue\"}, :fillOpacity {:value 0.4}, :stroke {:value \"steelblue\"}, :strokeWidth {:value 2}, :strokeOpacity {:value 1}}}}], :data [{:name \"d78c8e0e-4ae8-4ba0-a73a-bc88f26459b2\", :values ({:x 0.0, :y 0} {:x 0.08333333333333336, :y 0.6765} {:x 0.1666666666666667, :y 0.0} {:x 0.25000000000000006, :y 0.0} {:x 0.3333333333333334, :y 0.0} {:x 0.4166666666666668, :y 0.0} {:x 0.5000000000000001, :y 0.0} {:x 0.5833333333333335, :y 0.0} {:x 0.6666666666666669, :y 0.0} {:x 0.7500000000000002, :y 0.0} {:x 0.8333333333333336, :y 0.0} {:x 0.916666666666667, :y 0.0} {:x 1.0000000000000002, :y 0.3235} {:x 1.0833333333333335, :y 0})}], :width 400, :height 247.2188, :padding {:bottom 20, :top 10, :right 10, :left 50}}}"}
;; <=

;; **
;;; Not surprisingly this produces the indistinguishable results, however, in the case of larger Bayes nets this transformation will result in significantly higher inference performance before enumeration query and optimizing program transformations are included in the compiler.
;; **

;; @@
(def expr '(let [is-cloudy (sample (flip 0.5))
        
        is-raining (cond (= is-cloudy true ) 
                           (sample (flip 0.8))
                         (= is-cloudy false) 
                           (sample (flip 0.2)))
        sprinkler-dist (cond (= is-cloudy true) 
                            	(flip 0.1)
                         	(= is-cloudy false) 
                            	(flip 0.5))
        
        sprinkler true
        _ (observe sprinkler-dist sprinkler)
        
        wet-grass-dist (cond (and (= sprinkler true) 
                             (= is-raining true))  								  				       (flip 0.99)
                        (and (= sprinkler false) 
                             (= is-raining false))  								  				   (flip 0.0)
                        (or  (= sprinkler true) 
                             (= is-raining true))  								  					   (flip 0.9))
        
        wet-grass true
        _ (observe wet-grass-dist wet-grass)]
    
           (predict :s (hash-map :is-cloudy  is-cloudy 
                                 :is-raining is-raining 
                                 :sprinkler  sprinkler) )))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;user/expr</span>","value":"#'user/expr"}
;; <=

;; @@
(def ex (rest expr))
(first ex)
(first (rest ex))
(rest (rest ex))
(first (rest (rest expr)))
(rest (rest (rest expr)))
(rest (rest expr))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>predict</span>","value":"predict"},{"type":"html","content":"<span class='clj-keyword'>:s</span>","value":":s"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>hash-map</span>","value":"hash-map"},{"type":"html","content":"<span class='clj-keyword'>:is-cloudy</span>","value":":is-cloudy"},{"type":"html","content":"<span class='clj-symbol'>is-cloudy</span>","value":"is-cloudy"},{"type":"html","content":"<span class='clj-keyword'>:is-raining</span>","value":":is-raining"},{"type":"html","content":"<span class='clj-symbol'>is-raining</span>","value":"is-raining"},{"type":"html","content":"<span class='clj-keyword'>:sprinkler</span>","value":":sprinkler"},{"type":"html","content":"<span class='clj-symbol'>sprinkler</span>","value":"sprinkler"}],"value":"(hash-map :is-cloudy is-cloudy :is-raining is-raining :sprinkler sprinkler)"}],"value":"(predict :s (hash-map :is-cloudy is-cloudy :is-raining is-raining :sprinkler sprinkler))"}],"value":"((predict :s (hash-map :is-cloudy is-cloudy :is-raining is-raining :sprinkler sprinkler)))"}
;; <=

;; @@
(defn let? [expr] (= expr 'let))

(defn cond? [expr] (= expr 'cond))

(defn if? [expr] (= expr 'if))

(defn and? [expr] (= expr 'and))

(defn or? [expr] (= expr 'or))

(defn sample? [expr] (= expr 'sample))

(defn predict? [expr] (= expr 'predict))

(defn observe? [expr] (= expr 'observe))

(defn eq? [expr] (= expr 'eq))

(defn def? [expr] (= expr 'def))

; Structure of state is e.g. {:prob 0.3 :vars {...} :observes {...} :predicts {...}}

; NOTE: Scoping is too hard to implement

(defn parse-query [[first-term & rest-terms :as this-form] next-parse state vars]
  ; Branch on type of construct we are trying to parse
  (cond (let? first-term) (parse-let rest-terms next-parse state vars)
        (cond? first-term) (parse-cond rest-terms next-parse state vars)
        (if? first-term) (parse-if rest-terms next-parse state vars)
        (sample? first-term) (parse-sample rest-terms next-parse state vars)
        (predict? first-term) (parse-query (first next-parse) (rest next-parse) state vars)	; Ignore predict blocks
        (observe? first-term) (parse-observe rest-terms next-parse state vars)
        (def? first-term) (parse-def rest-terms next-parse state vars)
        (nil? this-parse) state
        :else (throw (Exception. "Unrecognised syntactic construction"))
  )
)

(defn parse-let [[first-term & rest-terms] next-parse state vars]
  ; Parse variable bindings and add them to the list of lazy variables
  (let [names (take-nth 2 first-term)
        bindings (take-nth 2 (rest first-term))
        new-vars (zipmap names bindings)]
    
    ; TODO: Need function to evaluate binding expressions, calling parse-sample
    
    (parse-query rest-terms next-parse state vars)
  )
))
  
(defn parse-let-bindings [var-bindings state vars]
  (let [var-binding (first var-bindings)
        rest-bindings (rest var-bindings)
        var-name (first var-binding)
        var-expr (rest var-binding)]
    (if (nil? var-bindings)
      [state vars]
      (cond )
    )
    
    ; Incrementally build the state
    (recur rest-bindings state vars))
)

(defn parse-cond [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  
)

(defn parse-if [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(defn parse-and [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(defn parse-or [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(defn parse-sample [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  ; TODO: Extract probability of flip
  (conj (parse... ...) (parse... ...))
  
)

(defn parse-predict [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(defn parse-observe [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(defn parse-eq [[first-term & rest-terms] [prob rand-vars predict-vars observe-vars :as state] vars]
  (float 1)
)

(parse-query expr [{} {}] [])
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;user/parse-eq</span>","value":"#'user/parse-eq"}
;; <=

;; @@
(zipmap ['a 'b 'c 'd 'e] [1 2 3 4 5])
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-map'>{</span>","close":"<span class='clj-map'>}</span>","separator":", ","items":[{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>e</span>","value":"e"},{"type":"html","content":"<span class='clj-long'>5</span>","value":"5"}],"value":"[e 5]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>d</span>","value":"d"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"}],"value":"[d 4]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>c</span>","value":"c"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"}],"value":"[c 3]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>b</span>","value":"b"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[b 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>a</span>","value":"a"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"[a 1]"}],"value":"{e 5, d 4, c 3, b 2, a 1}"}
;; <=

;; @@
(merge {'e 1003} (zipmap ['a 'b 'c 'd] [1 2 3 4]))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-map'>{</span>","close":"<span class='clj-map'>}</span>","separator":", ","items":[{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>a</span>","value":"a"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"[a 1]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>b</span>","value":"b"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"}],"value":"[b 2]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>c</span>","value":"c"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"}],"value":"[c 3]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>d</span>","value":"d"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"}],"value":"[d 4]"},{"type":"list-like","open":"","close":"","separator":" ","items":[{"type":"html","content":"<span class='clj-symbol'>e</span>","value":"e"},{"type":"html","content":"<span class='clj-long'>1003</span>","value":"1003"}],"value":"[e 1003]"}],"value":"{a 1, b 2, c 3, d 4, e 1003}"}
;; <=

;; @@
(list? (first '((a b) (c d))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>true</span>","value":"true"}
;; <=

;; @@
((1) 2 3 4)
;; @@

;; @@
(defn parse-let-bindings 
  ([{}] (float 2))
  ([var-bindings] (float 1))
)
;; @@

;; @@
(first {:a 2 :b 9})
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-vector'>[</span>","close":"<span class='clj-vector'>]</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-keyword'>:b</span>","value":":b"},{"type":"html","content":"<span class='clj-long'>9</span>","value":"9"}],"value":"[:b 9]"}
;; <=

;; **
;;; The functions that manipulate a stack with hierarchy. When pushing/popping/getting the last element it will do so on the deepest part of the last element. We will use this hierarchy to keep track of which function we are evaluating (that is, when the last element of the unevaluated arguments is an empty vector we know to apply the function operation).
;; **

;; @@
; Retrieve the first element from the inner-most list in a list of lists
(defn inner-first
  [x]
  (if (list? (first x))
    (if (empty? (first x))
      (first x)
      (inner-first (first x))
    )
    (first x)
  )
)

; Remove the first element from the inner-most list in a list of lists
(defn pop-inner-first
  [x]
  (if (list? (first x))
    (if (empty? (first x))
      (pop x)
      (conj (pop x) (pop-inner-first (first x)))
    )
    (pop x)
  )
)

; A helper function for the function below
(defn pop-inner-first-helper
  [x]
  (if (list? (first x))
    (conj (pop x) (pop-inner-first-helper (first x)))
    nil
  )
)

; Remove the inner-most list from the front of a list of lists
; Need to do this in two steps because conj will append a nil onto front when recursion terminates
(defn pop-inner-first-list
  [x]
  (pop-inner-first (pop-inner-first-helper x))
)

; Retrieve the inner-most list in a list of lists
(defn inner-first-list
  [x]
  (if (list? (first x))
    (inner-first-list (first x))
    x
  )
)

; Push an element onto the front of the inner-most list from a list of lists
(defn push-inner-first
  [x y]
  (if (list? (first x))
    (conj (pop x) (push-inner-first (first x) y))
    (conj x y)
  )
)

; Returns true if an expression is equal to an irreducible type
(defn no-more-eval?
  [x]
  (or (number? x) (= (type x) java.lang.Boolean))
)

(def x '(((1 2 3) 0.2 1 3 5) 2 3 (2 (3 2 1) 4 5 (4 2.3 4.1))))

;(pop-inner-first-list '(2 3 1 2 3))

;(inner-first-list (pop-inner-first-list '(((3 4 5 1) (2 2 1)) 3 4 5)))

;(conj '(1 2 3) '(4 5))

(push-inner-first (push-inner-first x ()) 1)

;(pop-inner-first-list x)
;(pop-inner-first-list (pop-inner-first-list x))
;; @@
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"(1)"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"}],"value":"((1) 1 2 3)"},{"type":"html","content":"<span class='clj-double'>0.2</span>","value":"0.2"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>5</span>","value":"5"}],"value":"(((1) 1 2 3) 0.2 1 3 5)"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>3</span>","value":"3"},{"type":"html","content":"<span class='clj-long'>2</span>","value":"2"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"(3 2 1)"},{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-long'>5</span>","value":"5"},{"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>4</span>","value":"4"},{"type":"html","content":"<span class='clj-double'>2.3</span>","value":"2.3"},{"type":"html","content":"<span class='clj-double'>4.1</span>","value":"4.1"}],"value":"(4 2.3 4.1)"}],"value":"(2 (3 2 1) 4 5 (4 2.3 4.1))"}],"value":"((((1) 1 2 3) 0.2 1 3 5) 2 3 (2 (3 2 1) 4 5 (4 2.3 4.1)))"}
;; <=

;; @@
(def expr '(+ (- 5 2) (* 6 2)))

(defn eval-expr
  ; First case takes an expression and initializes the stacks with it
  ([expr]
   ; DEBUG: Print expression coming in as input
   (let [_ (println "Calling eval-expr with arguments\nexpr:" expr "\n")]
   	(eval-expr (list (first expr)) (rest expr) () true 10)
   ))
  
  ; Second case for when we have a nonempty stack
  ([operators unevaluated-arguments evaluated-arguments first-inside steps]
   (let [last-uneval-arg (first unevaluated-arguments)
         _ (println "Calling eval-expr with arguments")
         _ (println "operators:" operators)
         _ (println "unevaluated-arguments:" unevaluated-arguments)
         _ (println "evaluated-arguments:" evaluated-arguments)
         _ (println "last-uneval-arg:" last-uneval-arg "\n")]
     (cond
       ; Terminate when steps becomes zero (so as to stop infinite loops while debugging)
       (<= steps 0)
       	nil
       
       ; Otherwise, move unevaluated argument to evaluated list (e.g. if number)
       (no-more-eval? last-uneval-arg)
       (let [_ (println "moving unevaluated argument to evaluated list\n")
             new-unevaluated-arguments (pop-inner-first unevaluated-arguments)]
       	(eval-expr operators  (push-inner-first evaluated-arguments last-uneval-arg) (dec steps))
       )
       
       ; When there are no more operators, return last evaluated argument
       (empty? operators)
       	(let [_ (println "returning last evaluated argument\n")]
       	  (inner-first evaluated-arguments)
        )
           
       (list? last-uneval-arg)
       	(if (empty? last-uneval-arg)
          ; When the last argument is an empty list, we have to apply function to evaluated arguments
          ; NOTE: Do I need to reverse order of evaluated arguments?
          (let [_ (println "applying function to evaluated arguments\n")
                _ (println "expr" (conj (inner-first-list evaluated-arguments) (first operators)))
                eval-val (eval (conj (inner-first-list evaluated-arguments) (first operators)))]
            (if (no-more-eval? eval-val)
              (eval-expr (pop operators) (push-inner-first (pop-inner-first unevaluated-arguments) eval-val) (pop-inner-first-list evaluated-arguments) (dec steps))
              (throw (Exception. "Functions must evaluate to numbers or booleans!"))
            )
          )
          
          ; When the last argument is a nonempty list, we have to add function application and arguments to stack
          (let [_ (println "decomposing unevaluated argument\n")]
            (eval-expr (conj operators (first last-uneval-arg)) (push-inner-first (pop-inner-first-list unevaluated-arguments) (rest last-uneval-arg)) (push-inner-first evaluated-arguments '()) (dec steps))
          )
        )
       
       ; Else throw error
       :else (throw (Exception. "Unexpected syntactic form!"))
  	 )
   )
  )
)

(eval-expr '(+ (* 2 3) 1))

(inner-first-list '(((+ (* 7 4) 3) 1)))
(inner-first-list '((1 1)))
;; @@
;; ->
;;; Calling eval-expr with arguments
;;; expr: (+ (* 2 3) 1) 
;;; 
;;; Calling eval-expr with arguments
;;; operators: (+)
;;; unevaluated-arguments: ((* 2 3) 1)
;;; evaluated-arguments: ()
;;; last-uneval-arg: (* 2 3) 
;;; 
;;; decomposing unevaluated argument
;;; 
;;; Calling eval-expr with arguments
;;; operators: (* +)
;;; unevaluated-arguments: ((2 3) 1)
;;; evaluated-arguments: (())
;;; last-uneval-arg: (2 3) 
;;; 
;;; decomposing unevaluated argument
;;; 
;;; Calling eval-expr with arguments
;;; operators: (2 * +)
;;; unevaluated-arguments: ((3) 1)
;;; evaluated-arguments: ((()))
;;; last-uneval-arg: (3) 
;;; 
;;; decomposing unevaluated argument
;;; 
;;; Calling eval-expr with arguments
;;; operators: (3 2 * +)
;;; unevaluated-arguments: (() 1)
;;; evaluated-arguments: (((())))
;;; last-uneval-arg: () 
;;; 
;;; applying function to evaluated arguments
;;; 
;;; expr (3)
;;; 
;; <-
;; =>
;;; {"type":"list-like","open":"<span class='clj-list'>(</span>","close":"<span class='clj-list'>)</span>","separator":" ","items":[{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"},{"type":"html","content":"<span class='clj-long'>1</span>","value":"1"}],"value":"(1 1)"}
;; <=

;; @@
(no-more-eval? '(()))

(number? '(()))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-unkown'>false</span>","value":"false"}
;; <=

;; @@

;; @@
